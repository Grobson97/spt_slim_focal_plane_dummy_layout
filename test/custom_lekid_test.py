import gdspy
from models.custom_lekid import CustomLEKID


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create LEKID cell to add module cells to
    main_cell = library.new_cell("Main")

    test_lekid = CustomLEKID(
        coupler_length=500.0,
        idc_length=2000.0,
        idc_finger_number=60,
        signal_input_x=0.0,
        signal_input_y=0.0,
    )

    test_lekid_cell = test_lekid.make_cell(
        back_etch_layer=0,
        inductor_layer=1,
        idc_layer=1,
        readout_layer=2,
        ground_layer=3,
        dielectric_layer=4,
        cell_name="LEKID",
    )
    main_cell.add(test_lekid_cell)

    print(test_lekid.get_readout_connection_x())
    print(test_lekid.get_readout_connection_y())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
