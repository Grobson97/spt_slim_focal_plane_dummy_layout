import gdspy
from models.inductor import Inductor


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create LEKID cell to add module cells to
    main_cell = library.new_cell("Main")

    test_inductor = Inductor(
        track_width=2.0,
        separation=12.0,
        inductor_width=514.0,
        idc_end_length=52.0,
        midpoint_end_length=62,
        bend_radius=6.0,
        u_bend_height=56.0,
        signal_input_connection_x=10,
        signal_input_connection_y=23,
        tolerance=0.01,
    )

    test_inductor_cell = test_inductor.make_cell(microstrip_layer=0)

    main_cell.add(test_inductor_cell)

    print(test_inductor.get_idc_connection_x())
    print(test_inductor.get_idc_connection_y())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
