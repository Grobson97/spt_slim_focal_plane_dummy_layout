import gdspy
from models.omt import OMT


def main():
    library = gdspy.GdsLibrary("OMT Library", unit=1e-06, precision=1e-09)
    # Create LEKID cell to add module cells to
    main_cell = library.new_cell("Main")

    omt_instance = OMT(
        diameter=2410,
        choke_width=545,
        dual_polarisation=True,
        center_x=0.0,
        center_y=0.0,
    )

    omt_cell = omt_instance.make_cell(
        antenna_layer=1, ground_layer=0, dielectric_layer=2, back_etch_layer=3
    )
    main_cell.add(omt_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
