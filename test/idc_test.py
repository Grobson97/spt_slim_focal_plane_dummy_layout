import gdspy
from models.idc import IDC
from models.idc import IdcFingers


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create LEKID cell to add module cells to
    main_cell = library.new_cell("Main")

    test_idc_fingers = IdcFingers(
        finger_number=8, finger_width=4.0, finger_gap=8.0, finger_length=500
    )

    test_idc = IDC(
        terminal_gap=8.0,
        terminal_width=8.0,
        terminal_finger_separation=16,
        idc_fingers=test_idc_fingers,
        inductor_connection_x=0.0,
        inductor_connection_y=0.0,
    )

    test_idc_cell = test_idc.make_cell(
        back_etch_layer=0,
        idc_layer=1,
        ground_layer=2,
        dielectric_layer=3,
        cell_name="IDC",
    )

    main_cell.add(test_idc_cell)

    print(test_idc.get_coupler_connection_x())
    print(test_idc.get_coupler_connection_y())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
