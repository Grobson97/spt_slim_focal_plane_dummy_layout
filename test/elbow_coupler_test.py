import gdspy
from models.elbow_coupler import ElbowCoupler


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create LEKID cell to add module cells to
    main_cell = library.new_cell("Main")

    test_elbow_coupler = ElbowCoupler(
        coupler_length=600.0,
        finger_width=4.0,
        finger_gap=8.0,
        readout_track_width=10.0,
        idc_track_width=10.0,
        length_to_idc=52.0,
        length_to_readout=80,
        idc_connection_x=10,
        idc_connection_y=17,
    )

    test_elbow_coupler_cell = test_elbow_coupler.make_cell(
        readout_line_layer=0, idc_layer=1, cell_name="Elbow Coupler"
    )

    main_cell.add(test_elbow_coupler_cell)

    print(test_elbow_coupler.get_readout_connection_x())
    print(test_elbow_coupler.get_readout_connection_y())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
