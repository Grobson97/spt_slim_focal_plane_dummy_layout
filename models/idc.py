import gdspy
from gdspy.library import Cell
import numpy as np


class IdcFingers:
    def __init__(
        self,
        finger_number: int,
        finger_width: float,
        finger_gap: float,
        finger_length: float,
    ) -> None:

        """
        Creates new instance of IdcFingers.

        :param float finger_number: Total Number of IDC fingers (must be even).
        :param float finger_width: Width of IDC fingers.
        :param float finger_gap: Gap around IDC fingers.
        :param float finger_length: IDC finger length.

        NB: Units will be defined by the cell library used.
        NB: It is best to match the label of the Finger instance to the IDC
        instance. e.g Finger1, and IDC1.
        """
        self.finger_number = finger_number
        self.finger_width = finger_width
        self.finger_gap = finger_gap
        self.finger_length = finger_length


class IDC:
    def __init__(
        self,
        terminal_gap: float,
        terminal_width: float,
        terminal_finger_separation: float,
        idc_fingers: IdcFingers,
        inductor_connection_x=0.0,
        inductor_connection_y=0.0,
    ) -> None:

        """
        Creates new instance of an IDC.

        :param float terminal_gap: Gap size between the ends of the prongs at the
        inductor join.
        :param float terminal_width: Width of the track of the end prongs and
        perimeter geometries.
        :param float terminal_finger_separation: Distance between the bottom of the IDC
        end prongs and the top of the first LH finger.
        :param IdcFinger idc_fingers: Instance of the IdcFinger class containing
        the number of fingers, finger width, finger gap and finger length.
        :param float inductor_connection_x: X-coordinate of the connection to the inductor geometry, located between
        the end prongs at the inductor join. Defaults to 0.0.
        :param float inductor_connection_y: Y-coordinate of the connection to the inductor geometry, located between
        the end prongs at the inductor join in the center of the track.
        Defaults to 0.0.

         NB: You must ensure that idc_fingers.finger_length is smaller than
         idc_width - 2*terminal_width.
         NB: It is best to match the label of the Finger instance to the IDC
         instance. e.g Finger1, and IDC1.
        """

        self.terminal_gap = terminal_gap
        self.terminal_width = terminal_width
        self.terminal_finger_separation = terminal_finger_separation
        self.idc_fingers = idc_fingers
        self.inductor_connection_x = inductor_connection_x
        self.inductor_connection_y = inductor_connection_y
        self.idc_width = (
            idc_fingers.finger_length + 2 * terminal_width + idc_fingers.finger_gap
        )

    def make_cell(
        self,
        back_etch_layer: int,
        idc_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="IDC",
    ):

        """
        :param: int back_etch_layer: GDSII layer for back etch geometry.
        Returns the gdspy Cell for a given IDC instance. The geometry is
        oriented such that the IDC fingers are parallel to the x axis. To add a
        rotation this must be done with the .CellReference() method after
        calling this method.

        :param back_etch_layer: GDSII layer for back etch layer.
        :param idc_layer: GDSII layer for IDC geometry.
        :param ground_layer: GDSII layer for ground plane.
        :param dielectric_layer: GDSII layer for dielectric layer.
        :param cell_name: Name to be given to the cell.
        """

        origin_x = self.inductor_connection_x
        origin_y = self.inductor_connection_y
        idc_width = self.idc_width
        terminal_gap = self.terminal_gap
        terminal_width = self.terminal_width
        terminal_finger_separation = self.terminal_finger_separation
        idc_fingers = self.idc_fingers

        # Create cell for complete IDC geometries to be added to.
        idc_cell = gdspy.Cell(cell_name)

        # Create inductor overlap pads:
        overlap_pad1 = gdspy.Rectangle(
            (origin_x - terminal_gap / 2 - 6.0, origin_y - terminal_width / 2),
            (origin_x - terminal_gap / 2, origin_y - 24),
            layer=idc_layer,
        )
        overlap_pad2 = gdspy.Rectangle(
            (origin_x + terminal_gap / 2 + 6.0, origin_y - terminal_width / 2),
            (origin_x + terminal_gap / 2, origin_y - 24),
            layer=idc_layer,
        )

        coupler_connection_y = (
            origin_y
            + terminal_width / 2
            + terminal_finger_separation
            + idc_fingers.finger_number * idc_fingers.finger_width
            + (idc_fingers.finger_number - 1) * idc_fingers.finger_gap
        )

        # Create IDC terminals:
        terminal_path1 = gdspy.FlexPath(
            points=[
                (origin_x - terminal_gap / 2, origin_y),
                (origin_x - idc_width / 2 + terminal_width / 2, origin_y),
                (origin_x - idc_width / 2 + terminal_width / 2, coupler_connection_y),
            ],
            width=terminal_width,
            ends="flush",
            corners="natural",
            layer=idc_layer,
        )

        terminal_path2 = gdspy.FlexPath(
            points=[
                (origin_x + terminal_gap / 2, origin_y),
                (origin_x + idc_width / 2 - terminal_width / 2, origin_y),
                (origin_x + idc_width / 2 - terminal_width / 2, coupler_connection_y),
            ],
            width=terminal_width,
            ends="flush",
            corners="natural",
            layer=idc_layer,
        )

        # Define x coordinates of each end of both the left and right fingers.
        right_finger_x1 = (
            origin_x - idc_width / 2 + terminal_width + idc_fingers.finger_gap
        )
        right_finger_x2 = origin_x + idc_width / 2 - terminal_width
        left_finger_x1 = origin_x - idc_width / 2 + terminal_width
        left_finger_x2 = (
            origin_x + idc_width / 2 - terminal_width - idc_fingers.finger_gap
        )

        y_initial = (
            origin_y + terminal_width / 2 + terminal_finger_separation
        )  # variable to track the y position of new each finger pair
        count = 0
        while count < idc_fingers.finger_number / 2:
            right_finger = gdspy.Rectangle(
                (right_finger_x1, y_initial),
                (right_finger_x2, y_initial + idc_fingers.finger_width),
                layer=idc_layer,
            )
            left_finger = gdspy.Rectangle(
                (
                    left_finger_x1,
                    y_initial + idc_fingers.finger_width + idc_fingers.finger_gap,
                ),
                (
                    left_finger_x2,
                    y_initial + 2 * idc_fingers.finger_width + idc_fingers.finger_gap,
                ),
                layer=idc_layer,
            )

            idc_cell.add([right_finger, left_finger])

            y_initial += 2 * idc_fingers.finger_width + 2 * idc_fingers.finger_gap
            count += 1

        # Add negative hole in ground layer:
        idc_gnd_hole = gdspy.Rectangle(
            (origin_x - idc_width / 2 - 50, origin_y - terminal_width / 2 - 20),
            (origin_x + idc_width / 2 + 50, coupler_connection_y + 108),
            layer=ground_layer,
        )
        # Add negative hole in dielectric layer:
        idc_dielectric_hole = gdspy.Rectangle(
            (origin_x - idc_width / 2 - 50, origin_y - terminal_width / 2 - 20),
            (origin_x + idc_width / 2 + 50, coupler_connection_y + 108),
            layer=dielectric_layer,
        )

        # Add negative hole in back etch layer:
        back_etch_hole = gdspy.Rectangle(
            (origin_x - idc_width / 2 - 50, origin_y - terminal_width / 2 - 20),
            (origin_x + idc_width / 2 + 50, coupler_connection_y + 108),
            layer=back_etch_layer,
        )

        # Add remaining geometries to cell:
        idc_cell.add(
            [
                overlap_pad1,
                overlap_pad2,
                terminal_path1,
                terminal_path2,
                idc_gnd_hole,
                idc_dielectric_hole,
                back_etch_hole,
            ]
        )

        return idc_cell

    def get_coupler_connection_x(self):

        """
        Function to return the x-coordinate of IDC-Coupler join, located at the
        center of the last RH finger section.
        """

        inductor_connection_x = self.inductor_connection_x
        idc_width = self.idc_width
        terminal_width = self.terminal_width

        coupler_connection_x = (
            inductor_connection_x - idc_width / 2 + terminal_width / 2
        )

        return coupler_connection_x

    def get_coupler_connection_y(self):

        """
        Function to return the y-coordinate of IDC-Coupler join, located at the
        bottom left of the last RH finger section.
        """

        inductor_connection_y = self.inductor_connection_y
        terminal_width = self.terminal_width
        terminal_finger_separation = self.terminal_finger_separation
        idc_fingers = self.idc_fingers

        coupler_connection_y = (
            inductor_connection_y
            + terminal_width / 2
            + terminal_finger_separation
            + idc_fingers.finger_number * idc_fingers.finger_width
            + (idc_fingers.finger_number - 1) * idc_fingers.finger_gap
        )

        return coupler_connection_y
