import gdspy
from gdspy.library import Cell
import numpy as np


class ElbowCoupler:
    def __init__(
        self,
        coupler_length: float,
        finger_width: float,
        finger_gap: float,
        readout_track_width: float,
        idc_track_width: float,
        length_to_idc: float,
        length_to_readout: float,
        idc_connection_x=0.0,
        idc_connection_y=0.0,
    ) -> None:

        """
        Creates new instance of 2 pair elbow coupler with 4 fingers (4F). NB: All dimensions
        must be in micrometres.

        :param float coupler_length: Length of the coupler fingers.
        :param float finger_width: Width of the coupler fingers.
        :param float finger_gap: Gap with between coupler fingers.
        :param float readout_track_width: Width of path to and terminal on readout side.
        :param float idc_track_width: Width of path and terminal on idc side.
        :param float length_to_idc: Distance from fingers section to start of IDC.
        :param float length_to_readout: Distance from fingers section to middle
        of feedline.
        :param float idc_connection_x: X coordinate of center of IDC join. Default= 0.0.
        :param float idc_connection_y: Y coordinate of center of IDC join. Default= 0.0.
        """

        self.coupler_length = coupler_length
        self.finger_width = finger_width
        self.finger_gap = finger_gap
        self.readout_track_width = readout_track_width
        self.idc_track_width = idc_track_width
        self.length_to_idc = length_to_idc
        self.length_to_readout = length_to_readout
        self.idc_connection_x = idc_connection_x
        self.idc_connection_y = idc_connection_y

    def make_cell(
        self, readout_line_layer: int, idc_layer: int, cell_name="Elbow Coupler"
    ) -> gdspy.Cell:

        """
        Returns the gdspy Cell for a given Coupler instance. The geometry is
        oriented such that the coupler fingers are parallel to the x axis
        and the coupler connects to the idc on the top and feedline on the
        bottom.

        :param int readout_line_layer: GDSII layer for the readout feedline.
        :param int idc_layer: GDSII layer for the IDC.
        :param str cell_name: Name to be used to reference the cell.
        """

        coupler_length = self.coupler_length
        finger_width = self.finger_width
        finger_gap = self.finger_gap
        readout_track_width = self.readout_track_width
        idc_track_width = self.idc_track_width
        length_to_idc = self.length_to_idc
        length_to_readout = self.length_to_readout
        idc_connection_x = self.idc_connection_x
        idc_connection_y = self.idc_connection_y

        # Create cell to add coupler geometry to
        coupler_cell = gdspy.Cell(cell_name)

        # Create rectangle between IDC and coupler finger.
        idc_to_coupler_geometry = gdspy.Rectangle(
            ((idc_connection_x - idc_track_width / 2), (idc_connection_y)),
            (
                (idc_connection_x + idc_track_width / 2),
                (idc_connection_y + length_to_idc),
            ),
            layer=idc_layer,
        )
        # Add geometry to coupler cell.
        coupler_cell.add(idc_to_coupler_geometry)

        # Define points for readout coupler section.
        readout_coupler_points = [
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap),
                (idc_connection_y + length_to_idc),
            ),
            (
                (
                    idc_connection_x
                    + idc_track_width / 2
                    + finger_gap
                    + coupler_length
                    + readout_track_width
                ),
                (idc_connection_y + length_to_idc),
            ),
            (
                (
                    idc_connection_x
                    + idc_track_width / 2
                    + finger_gap
                    + coupler_length
                    + readout_track_width
                ),
                (idc_connection_y + length_to_idc + 4 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap + coupler_length),
                (idc_connection_y + length_to_idc + 4 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap + coupler_length),
                (idc_connection_y + length_to_idc + 3 * finger_width + 2 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap),
                (idc_connection_y + length_to_idc + 3 * finger_width + 2 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap),
                (idc_connection_y + length_to_idc + 2 * finger_width + 2 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap + coupler_length),
                (idc_connection_y + length_to_idc + 2 * finger_width + 2 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap + coupler_length),
                (idc_connection_y + length_to_idc + finger_width),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap),
                (idc_connection_y + length_to_idc + finger_width),
            ),
        ]
        # Create readout coupler geometry
        readout_coupler_geometry = gdspy.Polygon(
            readout_coupler_points, layer=readout_line_layer
        )
        # Add readout_coupler_geometry to coupler_cell
        coupler_cell.add(readout_coupler_geometry)

        # Define points for double finger coupler section.
        idc_coupler_points = [
            (
                (idc_connection_x - idc_track_width / 2),
                (idc_connection_y + length_to_idc),
            ),
            (
                (idc_connection_x - idc_track_width / 2),
                (idc_connection_y + length_to_idc + 4 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + coupler_length),
                (idc_connection_y + length_to_idc + 4 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + coupler_length),
                (idc_connection_y + length_to_idc + 3 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2),
                (idc_connection_y + length_to_idc + 3 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2),
                (idc_connection_y + length_to_idc + 2 * finger_width + finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + coupler_length),
                (idc_connection_y + length_to_idc + 2 * finger_width + finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + coupler_length),
                (idc_connection_y + length_to_idc + finger_width + finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2),
                (idc_connection_y + length_to_idc + finger_width + finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2),
                (idc_connection_y + length_to_idc),
            ),
        ]
        # Create IDC coupler geometry.
        idc_coupler_geometry = gdspy.Polygon(idc_coupler_points, layer=idc_layer)
        # add IDC coupler geometry to coupler_cell.
        coupler_cell.add(idc_coupler_geometry)

        # Create rectangle between readout coupler and feedline.
        coupler_to_readout_geometry = gdspy.Rectangle(
            (
                (
                    idc_connection_x
                    + idc_track_width / 2
                    + finger_gap
                    + coupler_length
                    + readout_track_width
                ),
                (idc_connection_y + length_to_idc + 4 * finger_width + 3 * finger_gap),
            ),
            (
                (idc_connection_x + idc_track_width / 2 + finger_gap + coupler_length),
                (
                    idc_connection_y
                    + length_to_idc
                    + 4 * finger_width
                    + 3 * finger_gap
                    + length_to_readout
                ),
            ),
            layer=readout_line_layer,
        )
        # Add geometry to coupler cell.
        coupler_cell.add(coupler_to_readout_geometry)

        return coupler_cell

    def get_readout_connection_x(self):

        """
        Function to return the x-coordinate of Coupler-Feedline join, located at
        the end of a track orthogonal to the coupler fingers in the centre of
        the track.
        """

        readout_connection_x = (
            self.idc_connection_x
            + self.idc_track_width / 2
            + self.finger_gap
            + self.coupler_length
            + self.readout_track_width / 2
        )

        return readout_connection_x

    def get_readout_connection_y(self):

        """
        Function to return the x-coordinate of Coupler-Feedline join, located at
        the end of a track orthogonal to the coupler fingers in the centre of
        the track.
        """

        readout_connection_y = (
            self.idc_connection_y
            + self.length_to_idc
            + 4 * self.finger_width
            + 3 * self.finger_gap
            + self.length_to_readout
        )

        return readout_connection_y
