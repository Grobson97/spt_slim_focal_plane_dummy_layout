import gdspy
import numpy as np


class BondPad:
    def __init__(
        self,
        axis_of_symmetry=0.0,
        chip_edge=0.0,
    ) -> None:

        """
        Creates a new instance of a bond pad.

        :param: axis_of_symmetry: x coordinate for the centre line of
        symmetry for the bond pad.
        :param: chip_edge: y coordinate for the edge of the chip.
        """

        self.axis_of_symmetry = axis_of_symmetry
        self.chip_edge = chip_edge

    def make_cell(
        self,
        microstrip_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="Bond Pad",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given BongPad instance.

        :param: microstrip_layer: GDSII layer for the microstrip.
        :param: ground_layer: GDSII layer for the ground plane.
        :param: dielectric_layer: GDSII layer for the dielectric.
        :param: cell_name: Name to be used to reference the cell.
        """
        origin_x = self.axis_of_symmetry
        origin_y = self.chip_edge

        # Create bond pad cell to add geometries to:
        bond_pad_cell = gdspy.Cell(cell_name)

        # Create hole in dielectric layer:
        dielectric_hole = gdspy.Rectangle(
            (origin_x - 262.5, origin_y + 87.5),
            (origin_x + 262.5, origin_y + 612.5),
            dielectric_layer,
        )
        bond_pad_cell.add(dielectric_hole)

        # Create hole in ground plane:
        ground_hole_points = [
            (origin_x - 562.5, origin_y),
            (origin_x - 562.5, origin_y + 600),
            (origin_x - 49.5, origin_y + 850),
            (origin_x + 49.5, origin_y + 850),
            (origin_x + 562.5, origin_y + 600),
            (origin_x + 562.5, origin_y),
        ]
        ground_hole = gdspy.Polygon(ground_hole_points, layer=ground_layer)
        bond_pad_cell.add(ground_hole)

        # Create microstrip bond pad:
        microstrip_bond_pad_points = [
            (origin_x - 250, origin_y + 100),
            (origin_x - 250, origin_y + 600),
            (origin_x - 27.5, origin_y + 850),
            (origin_x + 27.5, origin_y + 850),
            (origin_x + 250, origin_y + 600),
            (origin_x + 250, origin_y + 100),
        ]
        microstrip_bond_pad = gdspy.Polygon(
            microstrip_bond_pad_points, layer=microstrip_layer
        )
        bond_pad_cell.add(microstrip_bond_pad)

        return bond_pad_cell

    def get_feedline_connection_x(self) -> tuple:
        """
        Function to return the x coordinates at which to connect the feedline to
        the bond pad.
        """

        return self.axis_of_symmetry

    def get_feedline_connection_y(self) -> tuple:
        """
        Function to return the y coordinates at which to connect the feedline to
        the bond pad.
        """

        return self.chip_edge + 850
