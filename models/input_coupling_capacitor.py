import gdspy
import numpy as np


class InputCouplingCapacitor:
    def __init__(
        self,
        cap_in: float,
        feedline_connection_x=0.0,
        feedline_connection_y=0.0,
    ) -> None:

        """
        Creates a new instance of an input coupling capacitor. NB: All dimensions
        must be in micrometres.

        param: cap_in: Width of the variable plate capacitor in the coupler.
        param: input_connection_x: x-coordinate of the connection to the
        feedline.
        param: input_connection_y: y-coordinate of the connection to the
        feedline.

        """

        self.cap_in = cap_in
        self.feedline_connection_x = feedline_connection_x
        self.feedline_connection_y = feedline_connection_y

    def make_cell(
        self, microstrip_layer: int, ground_layer: int, cell_name="Input Coupler"
    ) -> gdspy.Cell:

        """
        Returns the gdspy Cell for a given input coupling capacitor instance.
        The geometry is oriented such that coordinates of the connection to the
        feedline has the largest y value and the connection to the resonator
        the lowest.

        :param: microstrip_layer: GDSII layer for microstirp geometry.
        :param: ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        :param: cell_name: Name to be used to reference the cell.
        """
        cap_in = self.cap_in
        origin_x = self.feedline_connection_x
        origin_y = self.feedline_connection_y

        # Define width of input microstrip.
        width = 2.0

        # Create resonator cell to add module cells to
        input_coupler_cell = gdspy.Cell(cell_name)

        input_path = gdspy.Path(width=width, initial_point=(origin_x, origin_y))

        # Add a segment to the path going in the '-y' direction.
        input_path.segment(length=8.25, direction="-y", layer=microstrip_layer)

        # Build capacitor paddle geometries.
        cap_in_geometry = gdspy.Rectangle(
            (origin_x - cap_in / 2, origin_y - 8.25),
            (origin_x + cap_in / 2, origin_y - 10.75),
            layer=microstrip_layer,
        )
        fixed_cap_geometry = gdspy.Rectangle(
            (origin_x - 8.5, origin_y - 13.75),
            (origin_x + 8.5, origin_y - 16.25),
            layer=microstrip_layer,
        )
        ground_capacitor_negative1 = gdspy.Rectangle(
            (origin_x - 11.0, origin_y - 4.25),
            (origin_x - 9, origin_y - 18.25),
            layer=ground_layer,
        )
        ground_capacitor_negative2 = gdspy.Rectangle(
            (origin_x - 9.0, origin_y - 4.25),
            (origin_x + 11.0, origin_y - 6.25),
            layer=ground_layer,
        )
        ground_capacitor_negative3 = gdspy.Rectangle(
            (origin_x + 9.0, origin_y - 6.25),
            (origin_x + 11.0, origin_y - 20.25),
            layer=ground_layer,
        )
        ground_capacitor_negative4 = gdspy.Rectangle(
            (origin_x - 11.0, origin_y - 18.25),
            (origin_x + 9.0, origin_y - 20.25),
            layer=ground_layer,
        )

        # Add the all the geometries to the cell.
        input_coupler_cell.add(input_path)
        input_coupler_cell.add(cap_in_geometry)
        input_coupler_cell.add(fixed_cap_geometry)
        input_coupler_cell.add(ground_capacitor_negative1)
        input_coupler_cell.add(ground_capacitor_negative2)
        input_coupler_cell.add(ground_capacitor_negative3)
        input_coupler_cell.add(ground_capacitor_negative4)

        return input_coupler_cell

    def get_resonator_connection_x(self) -> float:
        """
        Function to return the x-coordinate of the connection point to the
        resonator. NB: this will always be the same as the feedline
        connecion x-coordinate.
        """
        return self.feedline_connection_x

    def get_resonator_connection_y(self) -> float:
        """
        Function to return the y-coordinate of the connection point to the
        output coupler.
        """
        return self.feedline_connection_y - 16.25
