import gdspy
import numpy
import math


class Inductor:
    def __init__(
        self,
        track_width: float,
        separation: float,
        inductor_width: float,
        idc_end_length: float,
        midpoint_end_length: float,
        bend_radius: float,
        u_bend_height: float,
        signal_input_connection_x: float,
        signal_input_connection_y: float,
        tolerance=0.01,
    ):
        """
        Creates new instance of an inductor section.

        :param float track_width: Width of the inductor track.
        :param float separation: Separation distance between parallel paths.
        :param float inductor_width: Distance from outer left edge to outer right
        edge.
        :param float midpoint_end_length: Distance from the idc connection and the
        outer
        edge of the first bend of the inductor.
        :param float bend_radius: Radius to be used for the curved corners
        :param float u_bend_height: Height of an inductor U-bend from outer edge
        to outer edge.
        :param float signal_input_connection_x: X-coordinate of the signal
        input connectection point, located at the center of the track at the
        midpoint of the inductor.
        :param float signal_output_connection_y: Y-coordinate of the signal
        input connectection point, located at the center of the track at the
        midpoint of the inductor.
        :param float tolerance: Tolerance to define number of points
        approximating circle. Defaults to 0.01.
        """

        self.track_width = track_width
        self.separation = separation
        self.inductor_width = inductor_width
        self.idc_end_length = idc_end_length
        self.midpoint_end_length = midpoint_end_length
        self.bend_radius = bend_radius
        self.tolerance = tolerance
        self.u_bend_height = u_bend_height
        self.signal_input_connection_x = signal_input_connection_x
        self.signal_input_connection_y = signal_input_connection_y

    def make_cell(self, microstrip_layer: int, cell_name="Inductor") -> gdspy.Cell:

        """
        Returns the gdspy Cell for a given inductor instance. The geometry is
        oriented such that the IDC would be connected below, in Y-direction. To
        add arotation this must be done with the .CellReference() method after
        calling this method.
        """

        origin_x = self.signal_input_connection_x
        origin_y = self.signal_input_connection_y
        track_width = self.track_width
        separation = self.separation
        inductor_width = self.inductor_width
        idc_end_length = self.idc_end_length
        midpoint_end_length = self.midpoint_end_length
        bend_radius = self.bend_radius
        tolerance = self.tolerance
        u_bend_height = self.u_bend_height

        # Define the cell to add inductor geometries to.
        inductor_cell = gdspy.Cell(cell_name)

        # Defines points for the inductor midpoint section.
        midpoint_end_points = [
            ((origin_x - separation / 2 - track_width / 2), (origin_y + bend_radius)),
            ((origin_x - separation / 2 - track_width / 2), (origin_y)),
            ((origin_x - separation / 2 - track_width / 2 + bend_radius), (origin_y)),
            ((origin_x + separation / 2 + track_width / 2 - bend_radius), (origin_y)),
            ((origin_x + separation / 2 + track_width / 2), (origin_y)),
            ((origin_x + separation / 2 + track_width / 2), (origin_y + bend_radius)),
        ]
        midpoint_end_geometry = gdspy.FlexPath(
            midpoint_end_points,
            width=track_width,
            corners="circular bend",
            bend_radius=bend_radius,
            tolerance=tolerance,
            ends="flush",
            layer=microstrip_layer,
        )

        # Define a as the distance from origin_y to Y coordinate of corner
        a = midpoint_end_length
        # Define b as the distance in y between corners of a U-bend.
        b = u_bend_height - 2 * track_width - separation
        # Define c as the distance in y between idc ends to the Y coordinate of
        # the corner.
        c = idc_end_length

        inductor_points = [
            ((origin_x), (origin_y + bend_radius)),
            ((origin_x), (origin_y + a)),
            (
                (origin_x - inductor_width / 2 + track_width + separation / 2),
                (origin_y + a),
            ),
            (
                (origin_x - inductor_width / 2 + track_width + separation / 2),
                (origin_y + a + b),
            ),
            (
                (origin_x + inductor_width / 2 - track_width - separation / 2),
                (origin_y + a + b),
            ),
            (
                (origin_x + inductor_width / 2 - track_width - separation / 2),
                (origin_y + a + 2 * b),
            ),
            ((origin_x), (origin_y + a + 2 * b)),
            ((origin_x), (origin_y + a + 2 * b + c)),
        ]
        inductor_geometry = gdspy.FlexPath(
            inductor_points,
            width=[track_width, track_width],
            offset=separation + track_width,
            corners="circular bend",
            bend_radius=bend_radius,
            tolerance=tolerance,
            ends="flush",
            layer=microstrip_layer,
        )

        signal_input_overlap_pad = gdspy.Rectangle(
            (origin_x - 4.0, origin_y + track_width / 2),
            (origin_x + 4.0, origin_y + track_width / 2 - 10),
            layer=microstrip_layer,
        )

        inductor_cell.add(
            [midpoint_end_geometry, inductor_geometry, signal_input_overlap_pad]
        )

        return inductor_cell

    def get_idc_connection_x(self):

        """
        Function to return the X-coordinate of the central point between the two
        ends that would connect to the IDC.
        """

        return self.signal_input_connection_x

    def get_idc_connection_y(self):

        """
        Function to return the Y-coordinate of the central point between the two
        ends that would connect to the IDC.
        """

        origin_y = self.signal_input_connection_y
        midpoint_end_length = self.midpoint_end_length
        separation = self.separation
        track_width = self.track_width
        u_bend_height = self.u_bend_height
        idc_end_length = self.idc_end_length

        # Define a as the distance from origin_y to Y coordinate of corner
        a = midpoint_end_length
        # Define b as the distance in y between corners of a U-bend.
        b = u_bend_height - 2 * track_width - separation
        # Define c as the distance in y between idc ends to the Y coordinate of
        # the corner.
        c = idc_end_length

        idc_connection_y = origin_y + a + 2 * b + c

        return idc_connection_y
