from turtle import dot, width
import gdspy
from gdspy.library import Cell
import numpy as np
import math
import matplotlib.pyplot as plt
from models.filter_bank import FilterBank, is_even
from models.omt import OMT
from models.bond_pad import BondPad
from models.lekid import LEKID
from models.custom_lekid import CustomLEKID
from models.hybrid_180 import Hybrid180
from enum import Enum


# Define layers:
class Layer(Enum):
    BACK_ETCH = 0
    NIOBIUM_MICROSTRIP = 1
    ALUMINIUM_MICROSTRIP = 2
    SIN = 3
    NIOBIUM_GROUND = 4


library = gdspy.GdsLibrary("SPT-SLIM Layout Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = library.new_cell("Main")
