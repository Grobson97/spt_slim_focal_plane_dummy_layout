from turtle import dot, width
import gdspy
from gdspy.library import Cell
import numpy as np
import math
import matplotlib.pyplot as plt
from models.filter_bank import FilterBank, is_even
from models.omt import OMT
from models.bond_pad import BondPad
from models.lekid import LEKID
from models.custom_lekid import CustomLEKID
from models.hybrid_180 import Hybrid180
from enum import Enum

library = gdspy.GdsLibrary("Chip Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = library.new_cell("Main")

# Define layers:
class Layer(Enum):
    BACK_ETCH = 0
    NIOBIUM_MICROSTRIP = 1
    ALUMINIUM_MICROSTRIP = 2
    SIN = 3
    NIOBIUM_GROUND = 4


# define Antenna feedline and readout path widths:
feedline_width = 2.5
readout_width = 55.0
readout_gap = 22.0
chip_centre_to_edge = 13500
distance_to_spectrometer_centre = 6000

# Create cell for first chip:
chip_cell = gdspy.Cell("Chip")

# Create cell for common dual polarisation spectrometer footprint (OMT to filterbank):
dual_pol_spectrometer_cell = gdspy.Cell("Dual Pol Spectrometer")
# Create cell for common single polarisation spectrometer footprint (OMT to filterbank):
single_pol_spectrometer_cell = gdspy.Cell("Single Pol Spectrometer")
# Createe separate hybrid cell for one pixel to have no hybrid:
no_hybrid_spectrometer_cell = gdspy.Cell("No Hybrid Spectrometer")

# Create cell for each pixel (spectrometer plus LEKIDS):
pixel_cell_top = gdspy.Cell("Top Pixel")
pixel_cell_right = gdspy.Cell("Right Pixel")
pixel_cell_bottom = gdspy.Cell("Bottom Pixel")
pixel_cell_left = gdspy.Cell("Left Pixel")


def microstrip_connect_points(
    x1: float,
    y1: float,
    x2: float,
    y2: float,
    microstrip_width: float,
    bend_radius: float,
    microstrip_layer: int,
    target_cell: gdspy.Cell,
    x_first: bool,
) -> None:

    """
    Function to add a microstrip path between two points, (x1,y1) and (x2,y2).
    The microstrip path geometry is then added to the target_cell. The order of
    which axis is traversed first is defined by x_first. e.g. If True the path moves
    along the x-axis and then y.

    :param float x1: x coordinate of first point.
    :param float y1: y coordinate of first point.
    :param float x1: x coordinate of second point.
    :param float y1: y coordinate of second point.
    :param float microstrip_width: Width of microstrip path.
    :param float bend_radius: Radius of the 90 degree turn.
    :param int microstrip_layer: GDSII layer for microstrip.
    :param gdspy.Cell target_cell: The cell which the geometry will be added to.
    :param bool x_first: Boolean to determine which axis the path traverses
    first.
    """

    if x_first == True:
        mid_point = (x2, y1)
    if x_first == False:
        mid_point = (x1, y2)

    path = gdspy.FlexPath(
        points=[(x1, y1), mid_point, (x2, y2)],
        width=microstrip_width,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        layer=microstrip_layer,
    )

    target_cell.add(path)


def cpw_connect_points(
    x1: float,
    y1: float,
    x2: float,
    y2: float,
    centre_width: float,
    ground_gap: float,
    bend_radius: float,
    center_layer: int,
    ground_layer: int,
    target_cell: gdspy.Cell,
    x_first: bool,
) -> None:

    """
    Function to add a CPW path between two points, (x1,y1) and (x2,y2).
    The CPW path geometry is then added to the target_cell. The order of
    which axis is traversed first is defined by x_first. e.g. If True the path moves
    along the x-axis and then y.

    :param float x1: x coordinate of first point.
    :param float y1: y coordinate of first point.
    :param float x1: x coordinate of second point.
    :param float y1: y coordinate of second point.
    :param float centre_width: Width of CPW central path.
    :param float ground_gap: Gap width between centre and ground plane.
    :param float bend_radius: Radius of the 90 degree turn.
    :param int centre_layer: GDSII layer for CPW center.
    :param int ground_layer: GDSII layer for ground plane.
    :param gdspy.Cell target_cell: The cell which the geometry will be added to.
    :param bool x_first: Boolean to determine which axis the path traverses
    first.
    """

    if x_first == True:
        mid_point = (x2, y1)
    if x_first == False:
        mid_point = (x1, y2)

    path = gdspy.FlexPath(
        points=[(x1, y1), mid_point, (x2, y2)],
        width=[centre_width, centre_width + 2 * ground_gap],
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        layer=[center_layer, ground_layer],
    )

    target_cell.add(path)


################################################################################

# Section to create perforations bounding the chip in the back etch layer:

# Create cell to add all chip borders to:
chip_border_cell = gdspy.Cell("Chip Border")

# Create cell for fully etched chip border side:
non_perforated_edge_cell = gdspy.Cell("Non Perforated Edge")
non_perforated_edge = gdspy.Rectangle(
    (-14000, 50), (14000, -50), layer=Layer.BACK_ETCH.value
)
non_perforated_edge_cell.add(non_perforated_edge)

# Create cell to add sub perforations to to act as a reference cell:
perforated_edge_cell = gdspy.Cell("Perforated Edge")

# Sub sections of perforation:
chip_perforation1 = gdspy.Rectangle(
    (-14000, 50), (-6800, -50), layer=Layer.BACK_ETCH.value
)
chip_perforation2 = gdspy.Rectangle(
    (-6700, 50), (-50, -50), layer=Layer.BACK_ETCH.value
)
chip_perforation3 = gdspy.Rectangle((50, 50), (6700, -50), layer=Layer.BACK_ETCH.value)
chip_perforation4 = gdspy.Rectangle(
    (6800, 50), (14000, -50), layer=Layer.BACK_ETCH.value
)
perforated_edge_cell.add(
    [chip_perforation1, chip_perforation2, chip_perforation3, chip_perforation4]
)

# Make chip border around the chip:
chip_perforation_top = gdspy.CellReference(
    non_perforated_edge_cell, (0.0, 13550), rotation=0.0
)
chip_perforation_right = gdspy.CellReference(
    non_perforated_edge_cell, (13550, 0.0), rotation=90
)
chip_perforation_bottom = gdspy.CellReference(
    non_perforated_edge_cell, (0.0, -13550), rotation=0.0
)
chip_perforation_left = gdspy.CellReference(
    perforated_edge_cell, (-13550, 0.0), rotation=90
)

chip_border_cell.add(
    [
        chip_perforation_top,
        chip_perforation_right,
        chip_perforation_bottom,
        chip_perforation_left,
    ]
)

# Add all perforations to main chipA_cell:
chip_cell.add(chip_border_cell)

################################################################################

# Section to create OMT's:

# Create instance of dual pol OMT using omt module:
dual_pol_omt = OMT(
    diameter=2410,
    choke_width=545,
    dual_polarisation=True,
    center_x=0.0,
    center_y=0.0,
)
# Make OMT cell to be used as reference
dual_pol_omt_cell = dual_pol_omt.make_cell(
    antenna_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
    dielectric_layer=Layer.SIN.value,
    back_etch_layer=Layer.BACK_ETCH.value,
    cell_name="Dual Pol OMT",
)

# Create instance of single pol OMT using omt module:
single_pol_omt = OMT(
    diameter=2410,
    choke_width=545,
    dual_polarisation=False,
    center_x=0.0,
    center_y=0.0,
)
# Make OMT cell to be used as reference
single_pol_omt_cell = single_pol_omt.make_cell(
    antenna_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
    dielectric_layer=Layer.SIN.value,
    back_etch_layer=Layer.BACK_ETCH.value,
    cell_name="Single Pol OMT",
)

dual_pol_spectrometer_cell.add(dual_pol_omt_cell)
single_pol_spectrometer_cell.add(single_pol_omt_cell)

################################################################################

# Section to create 180 Hybrids:

hybrid_180 = Hybrid180(
    resonator_length=640,
    loop_width=2.0,
    sum_port_connection_x=2100,
    sum_port_connection_y=0.0,
)

hybrid_180_cell = hybrid_180.make_cell(
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value, cell_name="180 Hybrid"
)

sum_port_overlap = gdspy.Rectangle(
    point1=(
        hybrid_180.sum_port_connection_x,
        hybrid_180.sum_port_connection_y + feedline_width / 2,
    ),
    point2=(
        hybrid_180.sum_port_connection_x - 25.0,
        hybrid_180.sum_port_connection_y - feedline_width / 2,
    ),
    layer=Layer.NIOBIUM_MICROSTRIP.value,
)

# Add length of aluminium at sum port:
sum_port_path = gdspy.Path(
    width=feedline_width,
    initial_point=(
        hybrid_180.sum_port_connection_x - 20,
        hybrid_180.sum_port_connection_y,
    ),
    number_of_paths=1,
)
sum_path_bend_radius = 20
sum_path_bend_length = (0.5 * np.pi * sum_path_bend_radius) - (
    np.pi * feedline_width / 4
)
standard_length = 500

length_remaining = hybrid_180.resonator_length * 10
sum_port_path.segment(length=25, direction="-x", layer=Layer.ALUMINIUM_MICROSTRIP.value)
length_remaining = length_remaining - 25
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="r", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - sum_path_bend_length
sum_port_path.segment(
    length=standard_length, direction="+y", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - standard_length
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="ll", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - 2 * sum_path_bend_length
sum_port_path.segment(
    length=2 * standard_length + 2 * sum_path_bend_radius,
    direction="-y",
    layer=Layer.ALUMINIUM_MICROSTRIP.value,
)
length_remaining = length_remaining - 2 * standard_length + 2 * sum_path_bend_radius
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="rr", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - 2 * sum_path_bend_length
sum_port_path.segment(
    length=2 * standard_length + 2 * sum_path_bend_radius,
    direction="+y",
    layer=Layer.ALUMINIUM_MICROSTRIP.value,
)
length_remaining = length_remaining - 2 * standard_length + 2 * sum_path_bend_radius
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="ll", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - 2 * sum_path_bend_length
sum_port_path.segment(
    length=2 * standard_length + 2 * sum_path_bend_radius,
    direction="-y",
    layer=Layer.ALUMINIUM_MICROSTRIP.value,
)
length_remaining = length_remaining - 2 * standard_length + 2 * sum_path_bend_radius
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="rr", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - 2 * sum_path_bend_length
sum_port_path.segment(
    length=2 * standard_length + 2 * sum_path_bend_radius,
    direction="+y",
    layer=Layer.ALUMINIUM_MICROSTRIP.value,
)
length_remaining = length_remaining - 2 * standard_length + 2 * sum_path_bend_radius
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="ll", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - 2 * sum_path_bend_length
sum_port_path.segment(
    length=2 * standard_length + 2 * sum_path_bend_radius,
    direction="-y",
    layer=Layer.ALUMINIUM_MICROSTRIP.value,
)
length_remaining = length_remaining - 2 * standard_length + 2 * sum_path_bend_radius
sum_port_path.turn(
    radius=sum_path_bend_radius, angle="rr", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
length_remaining = length_remaining - 2 * sum_path_bend_length
sum_port_path.segment(
    length=length_remaining, direction="+y", layer=Layer.ALUMINIUM_MICROSTRIP.value
)
# length_remaining = length_remaining - 2 * sum_path_bend_length

hybrid_180_cell.add([sum_port_path, sum_port_overlap])


dual_pol_spectrometer_cell.add(hybrid_180_cell)
single_pol_spectrometer_cell.add(hybrid_180_cell)


# Connect OMT to hybrid:
omt_connections = dual_pol_omt.get_feedline_connections()

microstrip_connect_points(
    x1=omt_connections[0][0],
    y1=omt_connections[0][1],
    x2=hybrid_180.get_input_connection_A()[0],
    y2=hybrid_180.get_input_connection_A()[1],
    microstrip_width=feedline_width,
    bend_radius=500,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=dual_pol_spectrometer_cell,
    x_first=True,
)
microstrip_connect_points(
    x1=omt_connections[1][0],
    y1=omt_connections[1][1],
    x2=hybrid_180.get_input_connection_B()[0],
    y2=hybrid_180.get_input_connection_B()[1],
    microstrip_width=feedline_width,
    bend_radius=500,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=dual_pol_spectrometer_cell,
    x_first=True,
)

microstrip_connect_points(
    x1=omt_connections[0][0],
    y1=omt_connections[0][1],
    x2=hybrid_180.get_input_connection_A()[0],
    y2=hybrid_180.get_input_connection_A()[1],
    microstrip_width=feedline_width,
    bend_radius=500,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=single_pol_spectrometer_cell,
    x_first=True,
)
microstrip_connect_points(
    x1=omt_connections[1][0],
    y1=omt_connections[1][1],
    x2=hybrid_180.get_input_connection_B()[0],
    y2=hybrid_180.get_input_connection_B()[1],
    microstrip_width=feedline_width,
    bend_radius=500,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=single_pol_spectrometer_cell,
    x_first=True,
)

################################################################################

# Section to terminate unused OMT antenna in lossy paths

antenna_termination_cell = gdspy.Cell("Antenna Terminations")

y_length = abs(omt_connections[2][1]) + 300
offset = abs(omt_connections[2][1])

antenna_termination_path_points = [
    (0.0, 5),
    (0.0, y_length - offset),
    (40, y_length - offset),
    (40, -y_length - offset),
    (80, -y_length - offset),
    (80, y_length - offset),
    (120, y_length - offset),
    (120, -y_length - offset),
]

antenna_termination_path = gdspy.FlexPath(
    points=(antenna_termination_path_points),
    width=feedline_width,
    bend_radius=20,
    corners="circular bend",
    layer=Layer.ALUMINIUM_MICROSTRIP.value,
)

overlap = gdspy.Rectangle(
    point1=(-feedline_width / 2, 0.0),
    point2=(feedline_width / 2, 20),
    layer=Layer.NIOBIUM_MICROSTRIP.value,
)

antenna_termination_cell.add([antenna_termination_path, overlap])

antenna_termination_cell_reference1 = gdspy.CellReference(
    antenna_termination_cell,
    origin=(omt_connections[2][0], omt_connections[2][1]),
)
antenna_termination_cell_reference2 = gdspy.CellReference(
    antenna_termination_cell,
    origin=(omt_connections[3][0], omt_connections[3][1]),
    rotation=180.0,
    x_reflection=True,
)

dual_pol_spectrometer_cell.add(
    [antenna_termination_cell_reference1, antenna_termination_cell_reference2]
)


################################################################################

# section to create filter bank:

# Define target frequencies for filter bank filters:
# NB: Final filter is not included in the mask.
target_f0_array = np.array(
    [180, 170, 160, 155, 150.43, 150, 149.57, 140, 130, 120, 115]
)

# Create instance of filter bank using filter_bank module:
filter_bank = FilterBank(
    target_f0_array=target_f0_array,
    feedline_width=feedline_width,
    filter_separation="3/4",
    input_connection_x=3000,
    input_connection_y=0,
)

# Create filter bank cell to be used as a reference:
filter_bank_cell = filter_bank.make_cell(
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
)

dual_pol_spectrometer_cell.add(filter_bank_cell)
single_pol_spectrometer_cell.add(filter_bank_cell)


# Connect Hybrid to filterbank:
microstrip_connect_points(
    x1=hybrid_180.get_difference_port_connection()[0],
    y1=hybrid_180.get_difference_port_connection()[1],
    x2=filter_bank.input_connection_x - 600,
    y2=filter_bank.input_connection_y - 200,
    microstrip_width=feedline_width,
    bend_radius=10,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=dual_pol_spectrometer_cell,
    x_first=False,
)
microstrip_connect_points(
    x1=filter_bank.input_connection_x - 600,
    y1=filter_bank.input_connection_y - 200,
    x2=filter_bank.input_connection_x - 500,
    y2=filter_bank.input_connection_y - 100,
    microstrip_width=feedline_width,
    bend_radius=20,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=dual_pol_spectrometer_cell,
    x_first=True,
)
microstrip_connect_points(
    x1=filter_bank.input_connection_x - 500,
    y1=filter_bank.input_connection_y - 100,
    x2=filter_bank.input_connection_x,
    y2=filter_bank.input_connection_y,
    microstrip_width=feedline_width,
    bend_radius=20,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=dual_pol_spectrometer_cell,
    x_first=False,
)

# Connect Hybrid to filterbank:
microstrip_connect_points(
    x1=hybrid_180.get_difference_port_connection()[0],
    y1=hybrid_180.get_difference_port_connection()[1],
    x2=filter_bank.input_connection_x - 600,
    y2=filter_bank.input_connection_y - 200,
    microstrip_width=feedline_width,
    bend_radius=10,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=single_pol_spectrometer_cell,
    x_first=False,
)
microstrip_connect_points(
    x1=filter_bank.input_connection_x - 600,
    y1=filter_bank.input_connection_y - 200,
    x2=filter_bank.input_connection_x - 500,
    y2=filter_bank.input_connection_y - 100,
    microstrip_width=feedline_width,
    bend_radius=20,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=single_pol_spectrometer_cell,
    x_first=True,
)
microstrip_connect_points(
    x1=filter_bank.input_connection_x - 500,
    y1=filter_bank.input_connection_y - 100,
    x2=filter_bank.input_connection_x,
    y2=filter_bank.input_connection_y,
    microstrip_width=feedline_width,
    bend_radius=20,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=single_pol_spectrometer_cell,
    x_first=False,
)

################################################################################

# Section to add LEKIDs to each pixel:

used_f0_array = []
final_lekid_readout_connections = []


def connect_LEKIDs(
    lekid_f0_array: np.ndarray,
    filter_bank: FilterBank,
    filter_bank_cell: gdspy.Cell,
    target_cell: gdspy.Cell,
    target_cell_reference: str,
    upside_only=False,
    downside_only=False,
) -> None:
    """
    Function to add LEKID geometries on to each filter of a FilterBank geometry.
    Then add both the LEKID cells and the filter bank cell to the target cell.

    :param np.ndarray lekid_f0_array: Array of f0's in Hz for each LEKID.
    First f0 corresponds to the LEKID closest to the OMT. Must be same length as
    filter_bank.target_f0_array.
    :param FilterBank filter_bank: The FilterBank instance being connected to.
    :param gdspy.Cell filter_bank_cell: The cell containing the filter bank geometry.
    :param gdspy.Cell target_cell: The cell that will become the filterbank
    connected with LEKID's
    :param str target_cell_reference: Letter to use for reference in the cell name.
    :param bool upside_only: If True, LEKID's only placed on upside.
    :param bool downside_only: If True, LEKID's only place on downside.
    """

    for count, connection in enumerate(filter_bank.get_lekid_connections()):

        lekid = LEKID(
            target_f0=lekid_f0_array[count],
            signal_input_x=0.0,
            signal_input_y=0.0,
        )

        lekid_cell = lekid.make_cell(
            back_etch_layer=Layer.BACK_ETCH.value,
            inductor_layer=Layer.ALUMINIUM_MICROSTRIP.value,
            idc_layer=Layer.ALUMINIUM_MICROSTRIP.value,
            readout_layer=Layer.NIOBIUM_MICROSTRIP.value,
            ground_layer=Layer.NIOBIUM_GROUND.value,
            dielectric_layer=Layer.SIN.value,
            cell_name=("LEKID_" + target_cell_reference + str(count)),
        )

        if connection[2] == "up":
            lekid_rotation = 0.0
        if connection[2] == "down":
            lekid_rotation = 180.0

        if count == target_f0_array.size - 1:
            lekid_ref_cell = gdspy.CellReference(
                lekid_cell, (connection[0], connection[1]), rotation=lekid_rotation
            )
            used_f0_array.append(lekid_f0_array[count])
            target_cell.add(lekid_ref_cell)
            final_lekid_readout_connections.append(
                (lekid.get_readout_connection_x(), lekid.get_readout_connection_y())
            )
            continue  # Breaks loop if final lekid to avoid putting two lekids on the same place.

        if connection[2] == "up" and upside_only == True:
            lekid_ref_cell = gdspy.CellReference(
                lekid_cell, (connection[0], connection[1]), rotation=lekid_rotation
            )
            used_f0_array.append(lekid_f0_array[count])
        elif connection[2] == "down" and downside_only == True:
            lekid_ref_cell = gdspy.CellReference(
                lekid_cell, (connection[0], connection[1]), rotation=lekid_rotation
            )
            used_f0_array.append(lekid_f0_array[count])
        elif upside_only == True and downside_only == True:
            lekid_ref_cell = gdspy.CellReference(
                lekid_cell, (connection[0], connection[1]), rotation=lekid_rotation
            )
            used_f0_array.append(lekid_f0_array[count])

        # Continue if not LEKID for the first filter:
        if count == 0 and downside_only == False:
            continue

        target_cell.add(lekid_ref_cell)
    target_cell.add(filter_bank_cell)


################################################################################

# Section to create spectrometer cell without a hybrid and only one antenna
# feeding the filter bank and one with a lekid output.

no_hybrid_spectrometer_cell.add(dual_pol_omt_cell)
no_hybrid_spectrometer_cell.add(filter_bank_cell)

microstrip_connect_points(
    x1=omt_connections[0][0],
    y1=omt_connections[0][1],
    x2=filter_bank.input_connection_x - 1000,
    y2=filter_bank.input_connection_y + 650,
    microstrip_width=feedline_width,
    bend_radius=500,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=no_hybrid_spectrometer_cell,
    x_first=True,
)
microstrip_connect_points(
    x1=filter_bank.input_connection_x - 1000,
    y1=filter_bank.input_connection_y + 650,
    x2=filter_bank.input_connection_x,
    y2=filter_bank.input_connection_y,
    microstrip_width=feedline_width,
    bend_radius=500,
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    target_cell=no_hybrid_spectrometer_cell,
    x_first=False,
)

full_band_lekid = CustomLEKID(
    coupler_length=200.0,
    idc_length=2500.0,
    idc_finger_number=60,
    signal_input_x=0.0,
    signal_input_y=0.0,
)

full_band_lekid_cell = full_band_lekid.make_cell(
    back_etch_layer=Layer.BACK_ETCH.value,
    inductor_layer=Layer.ALUMINIUM_MICROSTRIP.value,
    idc_layer=Layer.ALUMINIUM_MICROSTRIP.value,
    readout_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
    dielectric_layer=Layer.SIN.value,
    cell_name="LEKID",
)

full_band_lekid_cell_reference = gdspy.CellReference(
    ref_cell=full_band_lekid_cell,
    origin=(omt_connections[1][0] + 100, -2284.6),
    rotation=0.0,
    x_reflection=True,
)

# Connect omt antenna to full band lekid with final microstrip width = 4.5um.
path_to_full_band_lekid = gdspy.Path(
    width=feedline_width,
    initial_point=(omt_connections[1][0], omt_connections[1][1]),
    number_of_paths=1,
)

path_to_full_band_lekid.segment(
    length=50, direction="+x", layer=Layer.NIOBIUM_MICROSTRIP.value
)
path_to_full_band_lekid.turn(
    radius=50.0, angle="r", layer=Layer.NIOBIUM_MICROSTRIP.value
)
path_to_full_band_lekid.segment(
    length=abs(-2284.6 - omt_connections[1][1] + 50),
    direction="-y",
    final_width=4.5,
    layer=Layer.NIOBIUM_MICROSTRIP.value,
)

no_hybrid_spectrometer_cell.add(
    [
        antenna_termination_cell_reference1,
        antenna_termination_cell_reference2,
        path_to_full_band_lekid,
    ]
)

pixel_cell_bottom.add(full_band_lekid_cell_reference)


################################################################################

# Connect lekids to filter banks:

# 4 groups separated by 45MHz, with LEKID's 15MHz apart:
top_pixel_LEKID_f0_array = np.linspace(1.5, 1.605, 15) * 1e9
right_pixel_LEKID_f0_array = np.linspace(1.65, 1.755, 15) * 1e9
bottom_pixel_LEKID_f0_array = np.linspace(1.8, 1.905, 15) * 1e9
left_pixel_LEKID_f0_array = np.linspace(1.95, 2.055, 15) * 1e9

connect_LEKIDs(
    lekid_f0_array=top_pixel_LEKID_f0_array,
    filter_bank=filter_bank,
    filter_bank_cell=single_pol_spectrometer_cell,
    target_cell=pixel_cell_top,
    target_cell_reference="A",
    upside_only=False,
    downside_only=True,
)
connect_LEKIDs(
    lekid_f0_array=right_pixel_LEKID_f0_array,
    filter_bank=filter_bank,
    filter_bank_cell=single_pol_spectrometer_cell,
    target_cell=pixel_cell_right,
    target_cell_reference="B",
    upside_only=False,
    downside_only=True,
)
connect_LEKIDs(
    lekid_f0_array=bottom_pixel_LEKID_f0_array,
    filter_bank=filter_bank,
    filter_bank_cell=no_hybrid_spectrometer_cell,
    target_cell=pixel_cell_bottom,
    target_cell_reference="C",
    upside_only=False,
    downside_only=True,
)
connect_LEKIDs(
    lekid_f0_array=left_pixel_LEKID_f0_array,
    filter_bank=filter_bank,
    filter_bank_cell=dual_pol_spectrometer_cell,
    target_cell=pixel_cell_left,
    target_cell_reference="D",
    upside_only=False,
    downside_only=True,
)

################################################################################

# Section to add dark LEKID's

dark_lekid_f0_array = np.array([1.45, 2.1]) * 1e9

dark_lekid_1 = LEKID(target_f0=1.45e9, signal_input_x=0.0, signal_input_y=0.0)
dark_lekid_1_cell = dark_lekid_1.make_cell(
    back_etch_layer=Layer.BACK_ETCH.value,
    inductor_layer=Layer.ALUMINIUM_MICROSTRIP.value,
    idc_layer=Layer.ALUMINIUM_MICROSTRIP.value,
    readout_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
    dielectric_layer=Layer.SIN.value,
    cell_name="Dark LEKID_1",
)
dark_lekid_1_cell_ref = gdspy.CellReference(
    dark_lekid_1_cell,
    origin=(dark_lekid_1.get_readout_connection_y() + readout_width / 2, 11500),
    rotation=90,
)

dark_lekid_2 = LEKID(target_f0=2.1e9, signal_input_x=0.0, signal_input_y=0.0)
dark_lekid_2_cell = dark_lekid_2.make_cell(
    back_etch_layer=Layer.BACK_ETCH.value,
    inductor_layer=Layer.ALUMINIUM_MICROSTRIP.value,
    idc_layer=Layer.ALUMINIUM_MICROSTRIP.value,
    readout_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
    dielectric_layer=Layer.SIN.value,
    cell_name="Dark LEKID_2",
)
dark_lekid_2_cell_ref = gdspy.CellReference(
    dark_lekid_2_cell,
    origin=(-dark_lekid_2.get_readout_connection_y() - readout_width / 2, -11500),
    rotation=-90,
)

chip_cell.add([dark_lekid_1_cell_ref, dark_lekid_2_cell_ref])

################################################################################

# Section to add the bond pads.

all_bond_pads_cell = gdspy.Cell("Bond Pads")

# Create bond pad instance:
bond_pad = BondPad(axis_of_symmetry=0.0, chip_edge=-chip_centre_to_edge)

bond_pad_cell = bond_pad.make_cell(
    microstrip_layer=Layer.NIOBIUM_MICROSTRIP.value,
    ground_layer=Layer.NIOBIUM_GROUND.value,
    dielectric_layer=Layer.SIN.value,
    cell_name="Bond Pad",
)

bond_pad1 = gdspy.CellReference(
    ref_cell=bond_pad_cell,
    origin=(0.0, 0.0),
    rotation=180,
)
bond_pad2 = gdspy.CellReference(ref_cell=bond_pad_cell, origin=(0.0, 0.0), rotation=0.0)

all_bond_pads_cell.add([bond_pad1, bond_pad2])

chip_cell.add(all_bond_pads_cell)

################################################################################

# Section to add readout line:

# Expression to use as the height of each lekid.
lekid_height = final_lekid_readout_connections[0][1]
#  height of filter. (length from centre of feedline to Lekid connection):
filter_height = feedline_width / 2 + 83.5

readout_path_points = [
    (bond_pad.get_feedline_connection_x(), bond_pad.get_feedline_connection_y()),
    (
        bond_pad.get_feedline_connection_x(),
        (
            -distance_to_spectrometer_centre
            - filter_height
            - lekid_height
            - readout_width / 2
            - 1500
        ),
    ),
    (
        bond_pad.get_feedline_connection_x() + 7400,
        (
            -distance_to_spectrometer_centre
            - filter_height
            - lekid_height
            - readout_width / 2
            - 1500
        ),
    ),
    (
        bond_pad.get_feedline_connection_x() + 7400,
        (
            -distance_to_spectrometer_centre
            - filter_height
            - lekid_height
            - readout_width / 2
        ),
    ),
    (
        bond_pad.get_feedline_connection_x()
        - distance_to_spectrometer_centre
        + filter_height
        + lekid_height
        + readout_width / 2,
        (
            -distance_to_spectrometer_centre
            - filter_height
            - lekid_height
            - readout_width / 2
        ),
    ),
    (
        bond_pad.get_feedline_connection_x()
        - distance_to_spectrometer_centre
        + filter_height
        + lekid_height
        + readout_width / 2,
        2500,
    ),
    (bond_pad.get_feedline_connection_x() + 1500, +2500),
    (bond_pad.get_feedline_connection_x() + 1500, -2500),
    (
        bond_pad.get_feedline_connection_x()
        + distance_to_spectrometer_centre
        - filter_height
        - lekid_height
        - readout_width / 2,
        -2500,
    ),
    (
        bond_pad.get_feedline_connection_x()
        + distance_to_spectrometer_centre
        - filter_height
        - lekid_height
        - readout_width / 2,
        (
            distance_to_spectrometer_centre
            + filter_height
            + lekid_height
            + readout_width / 2
        ),
    ),
    (
        bond_pad.get_feedline_connection_x() - 3800,
        (
            distance_to_spectrometer_centre
            + filter_height
            + lekid_height
            + readout_width / 2
        ),
    ),
    (
        bond_pad.get_feedline_connection_x() - 3800,
        (
            distance_to_spectrometer_centre
            + filter_height
            + lekid_height
            + readout_width / 2
            + 1500
        ),
    ),
    (
        bond_pad.get_feedline_connection_x(),
        (
            distance_to_spectrometer_centre
            + filter_height
            + lekid_height
            + readout_width / 2
            + 1500
        ),
    ),
    (bond_pad.get_feedline_connection_x(), -bond_pad.get_feedline_connection_y()),
]

readout_centre_path = gdspy.FlexPath(
    points=readout_path_points,
    width=readout_width,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=Layer.NIOBIUM_MICROSTRIP.value,
)

readout_gap_path = gdspy.FlexPath(
    points=readout_path_points,
    width=readout_width + 2 * readout_gap,
    offset=0.0,
    corners="circular bend",
    bend_radius=200,
    max_points=1999,
    layer=Layer.NIOBIUM_GROUND.value,
)


def bridge_builder(
    path_points: list,
    bridge_width: float,
    bridge_thickness: float,
    bridge_separation: float,
    bridge_layer: int,
) -> None:

    """
    Function to build bridges along the path of a CPW path, connecting the ground planes.
    Returns the cell of the bridges.

    :param list path_points: List of CPW path points.
    :param float bridge_width: Width of the bridges, equivalent to the total CPW
    ground plane gap width.
    :param float bridge_thickness: Thickness of the bridge, dimension parallel
    to path direction.
    :param float bridge_separation: Distance between adjacent bridges.
    :param int bridge_layer: GDSII layer for the bridges, should be the same
    as the ground planes.
    """

    bridge_cell = gdspy.Cell("Bridge")
    bridges_cell = gdspy.Cell("Bridges")

    bridge_cell.add(
        gdspy.Rectangle(
            point1=(-bridge_thickness / 2, bridge_width / 2),
            point2=(bridge_thickness / 2, -bridge_width / 2),
            layer=bridge_layer,
        )
    )

    for count, point in enumerate(path_points):

        # If to avoid going out of index at final point.
        if point != path_points[-1]:

            bridge_section_cell = gdspy.Cell("Bridge Section " + str(count))

            end_point = path_points[count + 1]

            x1 = point[0]
            y1 = point[1]
            x2 = end_point[0]
            y2 = end_point[1]

            # Block to calculate required rotation angle of the bridge:
            vector = [x2 - x1, y2 - y1]
            unit_vector = vector / np.linalg.norm(vector)
            x_unit_vector = [1.0, 0.0]
            dot_product = np.dot(unit_vector, x_unit_vector)
            cross_product = np.cross(x_unit_vector, vector)
            angle = np.arccos(dot_product) / np.pi * 180
            # If cross product is less than zero, angle is counter clockwise, hence correction:
            if cross_product < 0:
                angle = 360 - angle

            # Block to place bridges centrally between both points at a fixed distance:
            distance = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            number_of_bridges = math.floor(distance / bridge_separation)
            bridge_x_positions = np.linspace(
                start=1, stop=number_of_bridges, num=number_of_bridges
            )
            bridge_x_positions = (
                bridge_x_positions - np.sum(bridge_x_positions) / number_of_bridges
            )
            bridge_x_positions = distance / 2 + bridge_separation * bridge_x_positions

            # Loop to place bridges along the x axis at the required positions and spacings.
            for position in bridge_x_positions:
                bridge_cell_reference = gdspy.CellReference(
                    ref_cell=bridge_cell, origin=(position, 0.0), rotation=0.0
                )
                bridge_section_cell.add(bridge_cell_reference)

            # Create reference cell of bridge section and rotate to required angle.
            bridge_section_ref_cell = gdspy.CellReference(
                ref_cell=bridge_section_cell, origin=point, rotation=angle
            )
            bridges_cell.add(bridge_section_ref_cell)

    return bridges_cell


bridges_cell = bridge_builder(
    path_points=readout_path_points,
    bridge_width=2 * readout_gap + readout_width,
    bridge_thickness=6.0,
    bridge_separation=500,
    bridge_layer=Layer.NIOBIUM_GROUND.value,
)

# Boolean to remove bridges from readout_gap_path:
readout_gap_path = gdspy.boolean(
    operand1=readout_gap_path,
    operand2=bridges_cell,
    operation="not",
    layer=Layer.NIOBIUM_GROUND.value,
)

chip_cell.add([readout_centre_path, readout_gap_path])

################################################################################

# Section to add dowels to back etch layer:

# Add circle for centre dowel in back etch:
centre_dowel = gdspy.Round(center=(0.0, 0.0), radius=397, layer=Layer.BACK_ETCH.value)

alignment_dowel = gdspy.Path(width=794, initial_point=(9100, 0.0))
alignment_dowel.segment(length=1794, direction="+x")
alignment_dowel.fillet(radius=397)

chip_cell.add([centre_dowel, alignment_dowel])

################################################################################

# Section to remove small right angle from corners of ground plane and add labels:

ground_corner_cell = gdspy.Cell("Ground Corner")

ground_corner = gdspy.Polygon(
    points=[
        (-chip_centre_to_edge, (chip_centre_to_edge - 1000)),
        (-chip_centre_to_edge, chip_centre_to_edge),
        (-(chip_centre_to_edge - 1000), chip_centre_to_edge),
        (-(chip_centre_to_edge - 1000), (chip_centre_to_edge - 100)),
        (-(chip_centre_to_edge - 100), (chip_centre_to_edge - 100)),
        (-(chip_centre_to_edge - 100), (chip_centre_to_edge - 1000)),
    ],
    layer=Layer.NIOBIUM_GROUND.value,
)
ground_corner_cell.add(ground_corner)

ground_corner_top_left = gdspy.CellReference(
    ground_corner_cell, origin=(0.0, 0.0), rotation=0.0
)
ground_corner_top_right = gdspy.CellReference(
    ground_corner_cell, origin=(0.0, 0.0), rotation=-90.0
)
ground_corner_bottom_right = gdspy.CellReference(
    ground_corner_cell, origin=(0.0, 0.0), rotation=-180.0
)
ground_corner_bottom_left = gdspy.CellReference(
    ground_corner_cell, origin=(0.0, 0.0), rotation=-270.0
)
chip_cell.add(
    [
        ground_corner_top_left,
        ground_corner_top_right,
        ground_corner_bottom_right,
        ground_corner_bottom_left,
    ]
)

################################################################################

pixel_ref_cell_top = gdspy.CellReference(
    pixel_cell_top,
    (-distance_to_spectrometer_centre, distance_to_spectrometer_centre),
    rotation=0.0,
    x_reflection=True,
)
pixel_ref_cell_right = gdspy.CellReference(
    pixel_cell_right,
    (distance_to_spectrometer_centre, distance_to_spectrometer_centre),
    rotation=-90,
)
pixel_ref_cell_bottom = gdspy.CellReference(
    pixel_cell_bottom,
    (distance_to_spectrometer_centre, -distance_to_spectrometer_centre),
    rotation=-180,
    x_reflection=True,
)
pixel_ref_cell_left = gdspy.CellReference(
    pixel_cell_left,
    (-distance_to_spectrometer_centre, -distance_to_spectrometer_centre),
    rotation=-270,
)

chip_cell.add(
    [
        pixel_ref_cell_top,
        pixel_ref_cell_right,
        pixel_ref_cell_bottom,
        pixel_ref_cell_left,
    ]
)

spectrometer_info_label = gdspy.Text(
    "Freq range = 120-180 GHz\nR = 300",
    400,
    (4600, 12600),
    layer=Layer.NIOBIUM_GROUND.value,
)
chip_cell.add(spectrometer_info_label)

chip_A_cell = gdspy.CellReference(chip_cell, origin=(-16000, 16000))
chip_B_cell = gdspy.CellReference(chip_cell, origin=(16000, 16000))
chip_C_cell = gdspy.CellReference(chip_cell, origin=(-16000, -16000))
chip_D_cell = gdspy.CellReference(chip_cell, origin=(16000, -16000))

chip_A_label = gdspy.Text(
    "SPT-SLIM v1 - Dev A", 400, (-29000, 28600), layer=Layer.NIOBIUM_GROUND.value
)
chip_B_label = gdspy.Text(
    "SPT-SLIM v1 - Dev B", 400, (3000, 28600), layer=Layer.NIOBIUM_GROUND.value
)
chip_C_label = gdspy.Text(
    "SPT-SLIM v1 - Dev C", 400, (-29000, -3400), layer=Layer.NIOBIUM_GROUND.value
)
chip_D_label = gdspy.Text(
    "SPT-SLIM v1 - Dev D", 400, (3000, -3400), layer=Layer.NIOBIUM_GROUND.value
)

# main_cell.add([chip_A_cell])
# main_cell.add(chip_A_label)
main_cell.add([chip_A_label, chip_B_label, chip_C_label, chip_D_label])
main_cell.add([chip_A_cell, chip_B_cell, chip_C_cell, chip_D_cell])

gdspy.LayoutViewer(library=library, pattern={"default": 9})
# gdspy.write_gds("FBS_Prototype_device_single_input_mixed_pols.gds")

plt.figure(figsize=(8, 6))
plt.vlines(
    np.array(used_f0_array) * 1e-9, -20, 0, color="r", label="Spectrometer LEKIDs"
)
plt.vlines(dark_lekid_f0_array * 1e-9, -20, 0, color="b", Label="Dark LEKIDs")
plt.vlines(1.43, -20, 0, color="g", label="Pixel 4 Full Band LEKID")
plt.xlabel("Frequency (GHz)")
plt.ylabel("Readout (AU)")
plt.title("Expected f0 placements for each pixels LEKID's")
plt.legend()
plt.show()

print("LEKID F0's (GHz):")
print(np.array(used_f0_array) * 1e-9)

print("\n Filterbank F0's (GHz):")
print(target_f0_array[:-1])
